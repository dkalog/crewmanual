#!/usr/bin/env bash
git checkout fe7e58a3523fccd0aea3669717d0f49d15c52b59 docs/0990_about.md

git log -n 1 >> docs/0990_about.md

mkdocs build --clean

mkdocs2pandoc > encountersailingcrewmanual_text.pd
# Generate PDF
pandoc --latex-engine=xelatex --toc -f markdown+grid_tables+table_captions -o encountersailingcrewmanual_text.pdf encountersailingcrewmanual_text.pd

# Fix frontcover
cpdf encountersailingcrewmanual_frontcover.pdf encountersailingcrewmanual_text.pdf -o docs/images/encountersailingcrewmanual.pdf

git add docs/images/encountersailingcrewmanual.pdf
git commit docs/images/encountersailingcrewmanual.pdf  -m 'Update pdf'