Generate pdf

cd encountersailing/crewmanual
mkdocs2pandoc > encountersailingcrewmanual.pd
# Generate PDF
pandoc --toc -f markdown+grid_tables+table_captions -o encountersailingcrewmanual.pdf encountersailingcrewmanual.pd
# Generate EPUB
pandoc --toc -f markdown+grid_tables -t epub -o encountersailingcrewmanual.epub encountersailingcrewmanual.pd

# nanne on iPhone