# Spinnaker Pole up

In order to square the gennaker more outside the boat we often use the spinakker pole to bring the gennaker tack outside to luff. Normally the gennaker tack is attached to the tack-line which is secured on the bow of the boat under the pulpit.

Before  setting the boom, the guy is placed in the pole beak. When setting the boom we first lift the boom on the mast and then lift the toppinglift. Once the pole is at the proper height and trimmed horizontally we can start to pull the guy. As soon as there is enough tension on the guy the pit can slowly release tension from the tack line. The pit also has to release the downhaul. However whenever releasing the downhaul always keep tension on the downhaul and alway keep one wrap around the winch. The gennaker trimmer dictates the angle and height of the pole. The pole is always trimmed horizontally. When the gennaker is squared you can ease the gennaker sheet more.

### keypoints to remember
* bring pole end to proper board
* put guy in pole beak
* lift pole on mast
* lift pole end with topping lift
* keep tension on downhaul
* bring tension on guy
* release tackline slowly and carefully
* release downhaul slowly and carefully
* bring pole horizontal with toppinglift
* bring pole in position by grinding the guy
* ease the sheet to keep the gennaker in proper trim
* put more tension on downhaul