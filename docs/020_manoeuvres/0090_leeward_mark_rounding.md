# Leeward mark rounding

Rounding the leeward mark is the last manoeuvre in this chapter. Before starting this manoeuvre we should check if there is enough tension on the mainsail halyard. Normally we first have to remove the spinnaker pole. This manoeuvre is described in the chapter before the previous chapter. Once the tack of the gennaker is firmly secured to the bow of the boat by the tackline we are ready to start this manoeuvre. First the bowman brings the "retrieve line" to the pit and the line goes into the hatch. The retrieve line is attached to the middle of the gennaker. The next step is to hoist the genua.

       Sometimes the spinnaker pole stays squared.
       This is in situation when there is not a lot of wind
       or we are dead down wind.
       With other words in light conditions we sometimes
       hoist the genua and drop the gennaker
       before removing the spinnaker pole.
       This is the exception.

Hoisting the genua is described in the first chapter of the manoeuvring section. The difference is that we are now sailing downwind. Once again it is extremely important that there is enough slack on the sheet and that sail should always stay a little loose when it is up. The genua is trimmed with the bear away sheet. This specific sheet we have seen before at the weather mark. The genua is up the pole is removed and the gennaker is also still up. In situations where the pole is removed and the genua is up it is very difficult to keep the gennaker filled. This is primarily due to the fact that the Encounter does not have a bowsprit. Gennaker bowsprits did not exist back in the seventies. Due to the fact that the gennaker is not very effective when the genua is up we normally take the gennaker down right after the genua is up.

When bringing down the gennaker the big trick is to scare the gennaker by releasing the tackline totally or by spiking the tack with a metal marlin spike by the bowman. At the same time drop the gennaker halyard with at least 10 meters at once. And at the same time pull as hard as you can on the "retriever line". When all goes well the gennaker should be below deck before is realizes what's happening. To bring down the gennaker we have to pull the sail with at least three and preferable with four crewmembers. One is below deck, two are around the hatch and one is near the guard rail at the side of the boat. The trick is to move the sail along you and not into you, also always stay windward of the sail. The sail should be free to fly toward the back of the boat. In general this means that your back is towards the front of the boat. Once the gennaker is stowed below deck we are within two lengths of the buoy and everybody gets into the upwind race mode. The cunningham line is wrapped around the winch and within heavy conditions already tensioned. The driver makes a smooth turn around the buoy. Three things are important.

1. During the turn the boat should gain as much speed as possible.
2. At the end of the turn the boat should be close hauled.
3. There should be no room between the buoy and the boat at that point.

A couple of tricks help. Make a nice slow swing. Winch and grind in the sails with lots of power without overtrimming. Have all the waight already on luff with the legs overboard. Have the proper tension on the halyards, stays and back stay.

From genua hoist till being up to the upwind beat again is a long ride and we are really happy that you are still with us. Some of the above described manoeuvres we will repeat a couple of times again and then we really reach for the finish. And after the finish we all deserve a fresh cold beer. Cheers and we see you on the water.

### keypoints to remember
* tension on mainsail halyard
* spinnaker boom away
* retrieve line to pit and hatch
* genua up
* **DROP**
* spike gennaker tack
* release 10 meters of gennaker halyard
* pull on retrieve line
* bring sail down and below deck with at least three crewmembers
* two boatlenghts from buoy
* get into upwind mode
* all crewweight to luff
* turn boat
* trim main
* trim genua
* sail full speed close hauled along the mark

<!--
# Asymmetric drop

##Coordinating the drop
The bowman always stays forward. When the call is made to drop the left pitman releases the xxx Everyone – without task - helps to get the kite down. One moves below to become the sewer man during the douses.

It's important to control the halyard drop so that the kite is brought around to the weather side. The forward bowman concentrates on gathering the foot, making sure the entire length of it is on board, and the aft bowman gathers the belly (running a leech tape as he does so). The sewer grabs the belly and pulls it through the hatch (also running a leech tape to make sure it's coming in clean).
-->
