Before we describe the nine main manoeuvres during a med race we first give a description of a typical Med Race and some safety considerations for sailing on a BIG boat.

## Racing the Med
Racing on the Med is always in the sun and the wind is always gently blowing between two to four Beaufort. Since we are on a raceboat all velocities are measured in the same unit. The default unit for velocity on a boat is a knot. One knot is one sea mile per hour and a sea mile is 1852 meters. You can always ask me where that odd number comes from but it’s beyond scope to tell you here.

       1 knot  =  1852 meters/hour
       2 knots ≈  3600 meters/hour
       2 knots ≈     1 meter/second

If you spit in the water and count like clockwork you are able to estimate the speed of the boat quite accurate. You can also calculate the time it takes to get to the buoy when you know the distance to that buoy. The boat speed is always expressed in knots. It’s the most important number on the boat and for that reason you can always find that number back on a big display on the mast below the boom. If you look towards Paul you’re eyes will find that number as well.

But wait a minute we were talking about the wind. Yes and from now on we don’t talk about Beaufort anymore but we talk about knots when we discuss the velocity of the wind. The wind is always gently blowing between 5 to 15 knots in the Med. When blowing 15 knots we have upwind an apparent wind of 20 knots over deck and we are doing around 7.4 boatspeed. And if we are going downwind well you can figure that out by doing some basic trigonometry. Or just wait and see when you are on the racecourse.

The racecourse is normally around ten to fifteen miles long and goes up to a maximum of twenty miles. When you have at least a moderate breeze a race rarely takes more than three hours. In that time frame you go up and down wind and do around five buoy roundings. Normally you change course and headsails at the buoy. Apart from that you start by crossing a start-line and finish by crossing a finish-line. 
In proper order you have a pre-start preparations, the start, the upwind beat, the weather mark rounding, the downwind run, the leeward mark rounding and finally the finish. In this chapter we will mainly concentrate on the different manoeuvres during the race. As the title suggest we will only discuss the main manoeuvres. They are presented below in the order of appearance during the race.

* genua hoist
* tack
* gybe with genua or jib
* start
* weather mark rounding
* gybe with gennaker
* leeward mark rounding

Before getting to the manoeuvres in detail it is time a completely different topic. More Power Than You Can Handle.

# More Power Than You Can Handle
Sailing is always a balance between not enough power and more power than you can handle. We can consider three different wind conditions. Light wind, moderate wind and heavy wind.

* light wind    : True Wind Speed (TWS) < 5 knots
* moderate wind : 5 knots < TWS < 15 knots
* heavy wind    : TWS > 15 knots

In light wind conditions most of the time we have to deal with not enough power. In moderate conditions we constantly shift gears from not enough power to more power than we can handle. And finally with heavy wind conditions we have to deal most of the time with more power than we can handle. I like to talk bit more on **more power than you can handle**.

On BIG boats you have BIG loads. Encounter is a big boat. The boat weights about 18 tons. That is nine times the weight of daddy's Mercedes. Moreover on the backstauy we have up to four tons of tension, two times the weight of that Mercedes. And on the sheets up to two tons. That's on par with daddy's car.

The above sounds like a joke but loosing a finger or worse is not a joke. And You would not be the first one.

Without scaring you off. I would like to walk through some common-sense safety precautions.

First and foremost. Your safety and well being is more important than anything else. This leads to the follwoing three recommendations.

1. If you don't know what you are doing don't do it! Never make anything loose if you don't know what it is.
2. Always stay on board. Stay out of the way of booms, sheets, sails and careless other crewmembers.
3. If you really lost control and don't know what is going on. Please go below deck and only come back when you know what to do.

## Manoeuvres
In general there are two things to remember when doing a manoeuvre.

1. Be prepared.
2. Speed more than hurry.

Each manoeuvre is a sequence of steps which have to be conducted as a team working together. Within each manoeuvre there is a pivot point around which all the manoeuvre steps revolve. All the steps before that pivot point have to be executed as late as possible, however they should be finished well before that pivot point. Finishing in-time is more important than starting late. The steps after the pivot point are important too, however these steps are less time-critical. We will describe nine manoeuvres. The manoevres are presented in the order as they appear during a race. Of course some manoeuvres are repeated multiple times. Our race starts at hoisting the genua or jib about nine to seven minutes before the actaul starting signal.

1. genua hoist.
2. tack.
3. gybe with genua or jib.
4. start.
5. weather mark rounding.
6. gybe with gennaker.
7. spinnaker pole up.
8. spinnaker pole down.
9. leeward mark rounding.

This manual is primarily aimed at the occasional crew member. If you want detailed descriptions of each and every role I welcome you to read the earlier mentioned book “[The offshore crew’s manual](https://www.amazon.com/Offshore-Race-Crews-Manual/dp/1408157284/ref=sr_1_1?ie=UTF8&qid=1481373180&sr=8-1&keywords=The+offshore+crew%E2%80%99s+manual)" by Stuart Quarrie.
